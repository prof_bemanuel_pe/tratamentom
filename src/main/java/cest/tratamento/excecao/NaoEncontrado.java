package cest.tratamento.excecao;

public class NaoEncontrado extends Exception {
	@Override
	public String getMessage() {
		return "Item nao encontrado";
	}
}
