package cest.tratamento.excecao;

public class CodigoIncorreto extends Exception {
	@Override
	public String getMessage() {
		return "Codigo invalido";
	}
}
