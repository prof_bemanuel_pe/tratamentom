package cest.tratamento;

import java.sql.Connection;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import cest.tratamento.biblio.Biblioteca;

/**
 * Hello world!
 *
 */
public class App {
	Biblioteca b = new Biblioteca();
	Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		App a = new App();
		a.menu();
	}

	private void menu() {
		
		do {
			System.out.println("###################");
			System.out.println("Escolha:");
			System.out.println("[1] - Cadastrar");
			System.out.println("[2] - Consultar");
			System.out.println("[3] - Listar");
			System.out.println("###################");
			
			int opc = sc.nextInt();
			switch (opc) {
			case 1:
				registrar();
				break;
			case 2:
				try {
					b.consultar(sc);
					//apos erro
					//nada abaixo roda
					System.out.println("Linha a mais");
				} catch (InputMismatchException e) {
					JOptionPane.showMessageDialog(null, e);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);
				} finally {
					System.out.println("Sempre execute");
				}
				break;
			case 3:
				b.listar();
			default:
				break;
			}
		} while (true);


	}
	
	public void registrar() {
		Integer c = 0;
		String n = "";
		System.out.println("Codigo: ");
		c = sc.nextInt();
		System.out.println("Nome: ");
		sc.nextLine();
		n = sc.nextLine();
		b.registra(c, n);
	}

}
