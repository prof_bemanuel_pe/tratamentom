package cest.tratamento.biblio;

import java.util.ArrayList;
import java.util.Scanner;

import cest.tratamento.excecao.CodigoIncorreto;
import cest.tratamento.excecao.NaoEncontrado;

public class Biblioteca {
	ArrayList<Item> lista = new ArrayList<Item>();

	public boolean registra(Integer c, String n) {
		boolean res = false;
		Item l = new Livro(c, n);
		res = lista.add(l);
		System.out.println("Livro registrado");
		return res;
	}

	public void consultar(Scanner sc) throws CodigoIncorreto, NaoEncontrado {
		Integer c = 0;
		System.out.println("Codigo:");
		c = sc.nextInt();
		Item res = null;
		
		System.out.println("Pesquisando por:" + c);
		
		for (Item l : lista) {
			if (c.equals(l.getCodigo())) {
				System.out.println("Livro encontrado");
				System.out.println("Nome:" + l.getNome());
				res = l;
			}
		}
		if (res == null) throw new NaoEncontrado();
	}

	public void listar() {
		for (Item l : lista) {
			System.out.println(l);
		}
	}
}
