package cest.tratamento.biblio;

public class Livro implements Item {
	
	private int codigo;
	private String nome;
	
	
	public Livro(Integer cod, String n) {
		codigo = cod;
		nome = n;
	}

	@Override
	public Integer getCodigo() {
		return codigo;
	}

	@Override
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("---------------------\n");
		sb.append("Cod:" + codigo + "\n");
		sb.append("Nome:" + nome + "\n");
		return sb.toString();
	}

}
